%% Problem 1
clear; clc; close all;
%% givens
gamma = 1.4;
Pr = 0.75;
dEta = 1e-4;
maxEta = 20;

%% Read data
path = "C:\Users\mitch\OneDrive - UCB-O365\CUBoulder\ASEN5519\repo\asen5519\assignments\5";
fName = "BL_Summary_Tratio_02.dat";
M = readmatrix(fullfile(path,fName));

%% Initalize parameters
TwTe = 2;
Me = 10;
% find correct data in datafile
row = find(M(:,1) == Me);
col = find(M(1,:) == TwTe);
b_init = M(row,3);
e_init = M(row,4);
g = TwTe;
nExp = 0.72;

% inital vars
f = 0;
a = 0;
b_init = M(row,3);
C_init = g^(nExp-1);
e_init = M(row,4);
f_init = 0;
CDot_init = (nExp-1)*g^(nExp-2)*e_init;

%initalize vars
b = b_init;
C = C_init;
e = e_init;
f = f_init;
yMod(1) = 0;
TTe(1) = g;
UUe = f_init;
CDot = CDot_init;

%% Integrate
eta = 0:dEta:maxEta;
for n = 1:length(eta)
    % calculate differentials
    bDot(n) = -b(n)/C(n)*(CDot(n) + f(n));
    eDot(n) = -CDot(n)/C(n)*e(n) - Pr/C(n)*f(n)*e(n) - Pr*(gamma - 1)*Me^2*b(n)^2;
     
    % integration
    b(n+1) = bDot(n)*dEta + b(n);
    g(n+1) = e(n)*dEta + g(n);
    e(n+1) = eDot(n)*dEta + e(n);
    C(n+1) = g(n+1)^(nExp-1);
    CDot(n+1) = (nExp - 1)*g(n+1)^(nExp - 2)*e(n+1);
    a(n+1) = b(n)*dEta + a(n);
    f(n+1) = a(n)*dEta + f(n);
    
    % physical quantites
    TTe(n+1) = g(n+1);
    UUe(n+1) = a(n+1);
    yMod(n+1) = sqrt(2)*g(n)*dEta + yMod(n);
      
end

%% Plots
% check a and g limit to 1
figure
hold on
plot(a)
plot(g)
title('Both should converge to 1')
legend('a','g')

% temp
figure
plot(TTe,yMod)
title('Temperature')
xlabel('T/T_e')
ylabel('^{y sqrt(Re_x)}/_{x}')

% velocity
figure
plot(UUe,yMod)
title('Velocity')
xlabel('U/U_e')
ylabel('^{y sqrt(Re_x)}/_{x}')


