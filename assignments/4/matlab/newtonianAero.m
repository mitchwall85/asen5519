%% Newtonian Aerodynamics Calculator
% Mitch Wall
% ASEN 5519, assignement 4
clear; clc; close all;
%% Inputs
alp = 10; % AoA
uInf_mag = 1; % magnitude of freestream

%% read in grid
% grid_fname = 'Flat_Plate_Processed.dat';
grid_fname = 'Sphere_Processed.dat';
% grid_fname = 'Apollo_Processed.dat';

% ===== Read and Process Grid
% -- One row per node, first 3 nodes form 1st triangle, and so on
grid_raw = dlmread(grid_fname);
grid_raw(:,1) = grid_raw(:,1) - 0.01; % shift origin to be inside vehicle to make unit normal calculation easier


% check if the number of nodes makes sense
n_tri = size(grid_raw,1)/3;
if (mod(n_tri,1)~=0)
   error('Error, number of nodes is not divisible by 3\n');
end

% -- grid_x(k,j,i) has k-th dimension of j-th node of surf tri i
grid_x = zeros(3,3,n_tri); % XYZ coords of each node
center_x = zeros(3,n_tri); % XYZ coords of each element center
for i=1:n_tri
   for j=1:3
      grid_x(1:3,j,i) = grid_raw( 3*(i-1)+j, 1:3);
      center_x(:,i) = center_x(:,i) + grid_x(:,j,i)/3;
   end
end

% -- Node indexes that define each triangle: (1,2,3), (4,5,6), ...
% -- If grid format was more sophisticated than STL, this would change
tri_conn = zeros(n_tri,3);
for i=1:n_tri
   tri_conn(i,1:3) = [ 3*(i-1)+1, 3*(i-1)+2, 3*(i-1)+3 ];
end

%% Freestream flow
uInf = uInf_mag*[cosd(alp), sind(alp), 0];

%% find element information
% initalize quantities of interest
n = zeros(n_tri,3);
phi = zeros(n_tri,1);
Cp = zeros(n_tri,1);
A = zeros(n_tri,1);
F = zeros(n_tri,1);
for i=1:n_tri
    r12 = grid_x(:,2,i) - grid_x(:,1,i); % one vector along an element edge
    r13 = grid_x(:,3,i) - grid_x(:,1,i); % another vector along an element edge
    r23 = grid_x(:,3,i) - grid_x(:,2,i); 
    n(i,:) = cross(r12,r13); % normal vector
    if dot(n(i,:),grid_x(:,2,i)) < 0 % check direction is outwards
       n(i,:) = cross(r13,r12);
    end
    phi(i) = acos(dot(uInf,n(i,:))/norm(uInf)/norm(n(i,:))) - pi; % angle between normal and freestream (rad)
    % dot check to see if any force should exist
    if dot(uInf,n(i,:)) < 0  
        Cp(i) = 2*cos(phi(i))^2; % coeff of pressure 
    else
        Cp(i) = 0;
    end
    s = (norm(r12) + norm(r13) + norm(r23))/2; % semi perim
    A(i) = sqrt(s*(s-norm(r12))*(s-norm(r13))*(s-norm(r23)));    
    if min(A) < 0 % Error check #1
        error('Element surface area should never go below zero')
    end
    F(i) = Cp(i)*A(i);
    if min(F) < 0 % Error check #2
        error('Minimum force should never go below zero')
    end
    
end


%% -- Plot and label
trisurf(tri_conn, grid_raw(:,1), grid_raw(:,2), grid_raw(:,3), Cp);
title('Cp');
xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)');
colorbar; caxis([ 0 2 ]);