%% Problem 2, HW1 
% ASEN 5519
% Mitch Wall
clear; close all;
%%
% BallisticEquations = 1; % 1 = use ballistic equation simplifications, 0 = use full equations

Ra = 0.05; %m
Rb = 0.9; %m
A = pi*Rb*Rb; % m^2
m = 3400; % kg
CD = 0.4; % coeff. drag
L = 0; % m
T = 0; % TODO not sure what this is
k = 2e-4/sqrt(Ra); % [sqrt(kg)*sqrt(m)]
U0 = 4500; % m/s
h0 = 100e3; % m
gamma0 = -70; % deg, flight path angle
g = 9.81; % m/s^2
alp = 1.378e-4; % inverse scale height [1/m]
W = m*g; % weight [N]
rEarth = 6378.1e3; % radius of earth [m]
rho_SL = 1.225; % density at sea level [kg/m^3]

%% A
disp('Values from analitical model');
disp('q_dot [w/m^2], accel [g]')
% Max deceleration
% U = U0*exp(-1/2);
U_dot_max = U0^2*alp*sind(gamma0)/2*exp(-1)/g % check units [g]?

% Max Heating
Beta = -2*W*alp*sind(gamma0)/g/CD/A;
q_dot_max = k*sqrt(Beta/6)*exp(-1/2)*U0^3 % check units

%% B 
%% Full Eqns
% numerical solver for trajectory parameters
t0 = 0;
tF = 300; % [s]
dt = 1e-3; % [s]

% organize variables as the _inital value
U = U0; % inital speed
U_dot = 0; % inital decel
gamma = gamma0; % inital flight path angle
R = h0 + rEarth; % inital distance to center of earth % [m]
theta = 0; % inital theta is already zero
rho_init = rho_SL*exp(-alp*h0); % local density (kg/m^2]
q_dot = k*sqrt(rho_init)*U^3;

% loop over time, 1st order euler integration
i = 1;

if BallisticEquations == 0
    % loop for full equations
    while R-rEarth>0

        h = R(i) - rEarth; % altitude [m]
        rho = rho_SL*exp(-alp*h); % local density (kg/m^2]
        D = 1/2*CD*A*rho*U(i)^2; % drag force

        % trajectory state variables
        gamma = [gamma, gamma(i) + dt*g/U(i)*(L/W - (1 - U(i)^2/g/R(i)))*cosd(gamma(i))]; %#ok<*AGROW>
        U = [U, U(i) + -dt*g*(D/W + sind(gamma(i)) - T/W)];
        U_dot = [U_dot, -g*(D/W + sind(gamma(i)) - T/W)]; % [g]
        R = [R, R(i) + dt*U(i)*sind(gamma(i))];
        theta = [theta, theta(i) + dt*U(i)/R(i)*cosd(gamma(i))];
        q_dot = [q_dot, k*sqrt(rho)*U(i)^3];    

        i = i+1;
    end

U_full = U;
t_full = (0:dt:length(R)*dt - dt);
h_full = R - rEarth; % convert to altitude directly from R
    
%% Ballistic eqns
% numerical solver for trajectory parameters
t0 = 0;
tF = 300; % [s]
dt = 1e-3; % [s]

% organize variables as the _inital value
U = U0; % inital speed
U_dot = 0; % inital decel
gamma = gamma0; % inital flight path angle
R = h0 + rEarth; % inital distance to center of earth % [m]
theta = 0; % inital theta is already zero
rho_init = rho_SL*exp(-alp*h0); % local density (kg/m^2]
q_dot = k*sqrt(rho_init)*U^3;

% loop over time, 1st order euler integration
i = 1;

elseif BallisticEquations == 1
    % loop for ballistic simplifications
    while R-rEarth>0

        h = R(i) - rEarth; % altitude [m]
        rho = rho_SL*exp(-alp*h); % local density (kg/m^2]
        D = 1/2*CD*A*rho*U(i)^2; % drag force

        % trajectory state variables
        gamma = [gamma, gamma0]; % KEEP GAMMA CONSTANT HERE
        U = [U, U(i) + -dt*g*(D/W + sind(gamma(i)) - T/W)];
        U_dot = [U_dot, -D/m]; % [g] SIMPLIFICATIONS HERE
        R = [R, R(i) + dt*U(i)*sind(gamma(i))];
        theta = [theta, theta(i) + dt*U(i)/R(i)*cosd(gamma(i))];
        q_dot = [q_dot, k*sqrt(rho)*U(i)^3];    

        i = i+1;
        
        if i == 100e3
            break
        end
        
    end
    
end

h = R - rEarth; % convert to altitude directly from R
t = (0:dt:length(R)*dt - dt);

U_ball = U;
t_ball = (0:dt:length(R)*dt - dt);
h_ball = R - rEarth; % convert to altitude directly from R

%% C
% find max values

if BallisticEquations == 0
    disp('Full Equations Used')
elseif BallisticEquations ==1;
    disp('Ballistic Equations Used')
end

disp('Values from numerical model')
disp('q_dot [w/m^2], accel [g]')
[~,X] = max(abs(U_dot));
U_dot_max = U_dot(X)/g
[~,X] = max(abs(q_dot));
q_dot_max = q_dot(X)

%% Plots

figure()
hold on
plot(U_full,h_full)
plot(t_full,h_full)
plot(U_ball,h_ball)
plot(t_full,h_ball)

% Accel
% figure(1);
% plot(t,U_dot);
% ylabel('Acceleration [m/s^2]')
% xlabel('Time [s]')
% title('Acceleration vs. Time')
% 
% % Heating
% figure(2);
% plot(t,q_dot);
% ylabel('Heating [w/m^2]')
% xlabel('Time [s]')
% title('Heating vs. Time')

% % altitude
% figure(1);
% plot(t,h/1000);
% ylabel('Altitude [km]')
% xlabel('Time [s]')
% title('Altitude vs. Time')
% 
% % Velocity
% figure(1);
% plot(t,U);
% ylabel('Velocity [m/s]')
% xlabel('Time [s]')
% title('Velocity vs. Time')









