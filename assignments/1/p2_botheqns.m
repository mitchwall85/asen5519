%% Problem 2, HW1 
% ASEN 5519
% Mitch Wall
clear; clc; close all;
%%
% BallisticEquations = 1; % 1 = use ballistic equation simplifications, 0 = use full equations

Ra = 0.05; %m
Rb = 0.9; %m
A = pi*Rb*Rb; % m^2
m = 3400; % kg
CD = 0.4; % coeff. drag
L = 0; % thrust
T = 0; % lift
k = 2e-4/sqrt(Ra); % [sqrt(kg)*sqrt(m)]
U0 = 4500; % m/s
h0 = 100e3; % m
gamma0 = -70; % deg, flight path angle
g = 9.81; % m/s^2
alp = 1.378e-4; % inverse scale height [1/m]
W = m*g; % weight [N]
rEarth = 6378.1e3; % radius of earth [m]
rho_SL = 1.225; % density at sea level [kg/m^3]

%% A
disp('Values from analitical model');
disp('q_dot [w/m^2], accel [g]')
Beta = -2*W*alp*sind(gamma0)/g/CD/A;

% Max deceleration
U_dot_max = U0^2*alp*sind(gamma0)/2*exp(-1)/g % check units [g]?
U_accel_max = U0*exp(-1/2)
h_accel_max = (-1/alp)*log( (-Beta*log(U_accel_max/U0)/rho_SL))

% Max Heating
q_dot_max = k*sqrt(Beta/6)*exp(-1/2)*U0^3 % check units
U_q_dot_max = U0*exp(-1/6)
h_q_dot_max = (-1/alp)*log( (-Beta*log(U_q_dot_max/U0)/rho_SL))


%% B 
%% Full Eqns
% numerical solver for trajectory parameters
t0 = 0;
tF = 300; % [s]
dt = 1e-3; % [s]

% organize variables as the _inital value
U = U0; % inital speed
U_dot = 0; % inital decel
gamma = gamma0; % inital flight path angle
R = h0 + rEarth; % inital distance to center of earth % [m]
theta = 0; % inital theta is already zero
rho_init = rho_SL*exp(-alp*h0); % local density (kg/m^2]
q_dot = k*sqrt(rho_init)*U^3;

% loop over time, 1st order euler integration
i = 1;


% loop for full equations
while R-rEarth>0

    h = R(i) - rEarth; % altitude [m]
    rho = rho_SL*exp(-alp*h); % local density (kg/m^2]
    D = 1/2*CD*A*rho*U(i)^2; % drag force

    % trajectory state variables
    gamma = [gamma, gamma(i) + dt*g/U(i)*(L/W - (1 - U(i)^2/g/R(i)))*cosd(gamma(i))]; %#ok<*AGROW>
    U = [U, U(i) + -dt*g*(D/W + sind(gamma(i)) - T/W)];
    U_dot = [U_dot, U_dot(i) + -dt*(D/W + sind(gamma(i)) - T/W)]; % [g]
    R = [R, R(i) + dt*U(i)*sind(gamma(i))];
    theta = [theta, theta(i) + dt*U(i)/R(i)*cosd(gamma(i))];
    q_dot = [q_dot, k*sqrt(rho)*U(i)^3];    

    i = i+1;
end

U_full = U;
U_dot_full = U_dot;
q_dot_full = q_dot;
t_full = (0:dt:length(R)*dt - dt);
h_full = R - rEarth; % convert to altitude directly from R

    
%% Ballistic eqns
% numerical solver for trajectory parameters
clear('U','U_dot','gamma','R','theta','rho_init','q_dot')
% organize variables as the _inital value
U = U0; % inital speed
U_dot = 0; % inital decel
gamma = gamma0; % inital flight path angle
R = h0 + rEarth; % inital distance to center of earth % [m]
theta = 0; % inital theta is already zero
rho_init = rho_SL*exp(-alp*h0); % local density (kg/m^2]
q_dot = k*sqrt(rho_init)*U^3;

% loop over time, 1st order euler integration
i = 1;


% loop for ballistic simplifications
while R-rEarth>0

    h = R(i) - rEarth; % altitude [m]
    rho = rho_SL*exp(-alp*h); % local density (kg/m^2]
    D = 1/2*CD*A*rho*U(i)^2; % drag force

    % trajectory state variables
    gamma = [gamma, gamma0]; % KEEP GAMMA CONSTANT HERE
    U =     [    U, U(i)    + -dt*g*(D/W + sind(gamma(i)) - T/W)];
    U_dot = [U_dot, U_dot(i) + -dt*D/m]; % [g] SIMPLIFICATIONS HERE
%     U_dot = [U_dot, -D/m];
    R = [R, R(i) + dt*U(i)*sind(gamma(i))];
    theta = [theta, theta(i) + dt*U(i)/R(i)*cosd(gamma(i))];
    q_dot = [q_dot, k*sqrt(rho)*U(i)^3];    

    i = i+1;
end
    
U_ball = U;
U_dot_ball = U_dot;
q_dot_ball = q_dot;
t_ball = (0:dt:length(R)*dt - dt);
h_ball = R - rEarth; % convert to altitude directly from R

%% C
% find max values

disp('Values from numerical model')
disp('q_dot [w/m^2], accel [g]')
disp('full')
[~,X] = max(abs(U_dot_full));
full_Udot_max = U_dot_full(X)/g
full_Udot_max_speed = U_full(X)
full_Udot_max_height = h_full(X)

[~,X] = max(abs(q_dot_full));
q_dot_full_max = q_dot_full(X)
full_qdot_max_speed = U_full(X)
full_qdot_max_height = h_full(X)

disp('ball')
[~,X] = max(abs(U_dot_ball));
ball_Udot_max = U_dot_ball(X)/g
ball_Udot_max_speed = U_ball(X)
ball_Udot_max_height = h_ball(X)

[~,X] = max(abs(q_dot_ball));
q_dot_ball_max = q_dot_ball(X)
ball_qdot_max_speed = U_ball(X)
ball_qdot_max_height = h_ball(X)


%% Plots

figure()
hold on
ylabel('Altitude [km]')
xlabel('Speed [m/s]')
plot(U_full,h_full,'-','LineWidth',1)
plot(U_ball,h_ball,'--','LineWidth',1)
legend('Full','Ballistic')
saveas(gcf,'C:\Users\mitch\OneDrive - UCB-O365\CUBoulder\ASEN5519\repo\asen5519\assignments\1\figs\ptE_speed.png')

figure 
hold on
ylabel('Altitude [km]')
xlabel('Time [s]')
plot(t_full,h_full,'-','LineWidth',1)
plot(t_ball,h_ball,'--','LineWidth',1)
legend('Full','Ballistic')
saveas(gcf,'C:\Users\mitch\OneDrive - UCB-O365\CUBoulder\ASEN5519\repo\asen5519\assignments\1\figs\ptE_time.png')

% Accel
% figure(1);
% plot(t,U_dot);
% ylabel('Acceleration [m/s^2]')
% xlabel('Time [s]')
% title('Acceleration vs. Time')
% 
% % Heating
% figure(2);
% plot(t,q_dot);
% ylabel('Heating [w/m^2]')
% xlabel('Time [s]')
% title('Heating vs. Time')

% % altitude
% figure(1);
% plot(t,h/1000);
% ylabel('Altitude [km]')
% xlabel('Time [s]')
% title('Altitude vs. Time')
% 
% % Velocity
% figure(1);
% plot(t,U);
% ylabel('Velocity [m/s]')
% xlabel('Time [s]')
% title('Velocity vs. Time')









