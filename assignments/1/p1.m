%% Problem 1, HW1 
% ASEN 5519
% Mitch Wall
clear; clc; close all;
%%
SA = xlsread('atmosphereData.xlsx');
SA_h = SA(:,1); % Standard atmosphere altitude
SA_rho = SA(:,4); % Standard atmosphere density

%% exponential model
rho_SL = 1.225; %[kg/m^3]
alp = 1.378e-4; %[1/m]
rhoExp = rho_SL*exp(-alp*SA_h*1000);

%% Plot
figure
hold on
title('Density vs. Altitude')
ylabel('Altitude [km]')
xlabel('Density [kg/m^3]')
semilogx(SA_rho,SA_h)
semilogx(rhoExp,SA_h)
set(gca, 'XScale', 'log')
legend('Standard Atmosphere','Exponential Model')
saveas(gcf,'C:\Users\mitch\OneDrive - UCB-O365\CUBoulder\ASEN5519\repo\asen5519\assignments\1\figs\density.png')


